# -*- coding: utf-8 -*-
import os
from argparse import ArgumentParser
from configparser import ConfigParser

import arrow
from pymongo import MongoClient

from utility import build_query
from utility import query_from_mongo

"""
This script is meant to query from MongoDB and store the result in json format.
The fields in json are described in 'FIELDS' variable.

By default, the script will query documents for all topics as described in
'TOPICS' variable. User can optionally specify date range of query with
--start_date and --end_date parameters.

The number of documents and batch size are described for each topic.
 For example, if there are 13 topics, --train 2000 documents, --batch 10. It is
 expected that the script will query 2000 documents for each topic, and these
 documents are splitted into 10 batch files. So in train folder, there are 10
  files, each file contains (2000 / 10) * 13 = 2600 documents.
"""

CONFIG_LOC = "./config.ini"
DB_NAME = "training_data"
COLL_NAME = "english_corpus"
with open("./data/interim/topics.txt", 'r') as f:
    TOPICS = []
    for line in f.readlines():
        TOPICS.append(line.strip())

FIELDS = {"_id": 0,
          "title": 1,
          "text": 1,
          "tags": 1,
          "topic": 1}


def main(num_docs, download_path, batch_size, start_date, end_date):
    config = ConfigParser()
    config.read(CONFIG_LOC)
    mongo_uri = config.get("MONGO", "uri")
    mongo_con = MongoClient(mongo_uri, w=1)
    eng_corpus = mongo_con[DB_NAME][COLL_NAME]

    cursor_list = []
    for topic in TOPICS:
        query = build_query(topic, start_date, end_date)
        cursor_list.append(eng_corpus
                           .find(query, FIELDS)
                           .limit(sum(num_docs.values()))
                           .batch_size(batch_size))

    for mode, doc_size in num_docs.items():
        query_from_mongo(cursor_list, doc_size, batch_size, download_path, mode)


if __name__ == '__main__':
    args = ArgumentParser("Query raw data from MongoDB")
    args.add_argument("--train",
                      type=int,
                      action="store",
                      dest="train",
                      default=3000,
                      help="Number of docs for training of each topics")

    args.add_argument("--valid",
                      type=int,
                      action="store",
                      dest="valid",
                      default=1000,
                      help="Number of docs for validation of each topics")

    args.add_argument("--test",
                      type=int,
                      action="store",
                      dest="test",
                      default=1000,
                      help="Number of docs for test of each topics")

    args.add_argument("--batch",
                      type=int,
                      dest="batch_size",
                      default=1000,
                      help="Number of documents for each topic in one batch "
                           "file")

    args.add_argument("--download_path",
                      type=str,
                      action="store",
                      dest="download_path",
                      default=os.path.join(".", "data", "raw"),
                      help="Folder location to store the query")

    args.add_argument("--start_date",
                      type=str,
                      dest="start_date",
                      default=None,
                      help="Start date of documents with format YYYY-MM-DD")

    args.add_argument("--end_date",
                      type=str,
                      dest="end_date",
                      default=None,
                      help="End data of documents with format YYYY-MM-DD")

    parsed_args = args.parse_args()
    mode_docs = dict(zip(['train', 'valid', 'test'],
                         [parsed_args.train, parsed_args.valid,
                          parsed_args.test]))

    if parsed_args.start_date:
        start_date = arrow.get(parsed_args.start_date).to('UTC').datetime
    else:
        start_date = None

    if parsed_args.end_date:
        early_end_date = arrow.get(parsed_args.end_date).to('UTC')
        end_date = arrow.Arrow.span_range('day',
                                          early_end_date,
                                          early_end_date)[0][1].datetime
    else:
        end_date = arrow.now('UTC').datetime

    main(mode_docs, parsed_args.download_path, parsed_args.batch_size,
         start_date, end_date)
