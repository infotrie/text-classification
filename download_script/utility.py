# -*- coding: utf-8 -*-
import json
import os

import math

from tqdm import tqdm


def query_from_mongo(cursors, num_docs, batch_size, download_path, mode):
    """
    Query data from mongodb, split them into batches, then save them into
    json format. One batch file contains mixed of topics.
    :param cursors: List of MongoDB cursors
    :param num_docs: Number of documents per topic
    :param batch_size: The number of document in one batch
    :param download_path: Folder location to store the batch
    :param mode: 'train', 'valid', or 'test'
    :return: None
    """
    write_path = os.path.join(download_path, mode)
    if not os.path.exists(write_path):
        os.makedirs(write_path)
    epochs = int(math.ceil(num_docs / batch_size))
    for epoch in tqdm(range(epochs),
                      desc="#%s batches, %d documents for each topic"
                           % (mode, num_docs)):
        batch_docs = []
        for cursor in tqdm(cursors,
                           desc="#Topic"):
            for idx, doc in enumerate(cursor):
                batch_docs.append(doc)
                if idx >= batch_size:
                    break
        with open(os.path.join(write_path, "batch_%d.json" % epoch),
                  "w") as f:
            json.dump(batch_docs, f, indent=4, separators=(',', ': '))


def build_query(topic_name, start_date, end_date):
    """
    Build MongoDB query. It is assumed topic_name and end_date are exist.
    :param topic_name: Topic name for topic field in MongoDB collection
    :param start_date: Start date in datetime object
    :param end_date: End date in datetime object
    :return: MongoDB query in dictionary object
    """
    if not end_date or not topic_name:
        raise ValueError("Provide both topic_name and end_date")

    query = {"topic": topic_name}
    if start_date:
        query["published_date"] = {"$gte": start_date,
                                   "$lte": end_date}
    else:
        query["published_date"] = {"$lte": end_date}
    return query
