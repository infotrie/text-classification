Query Mongo to data folder

- [x] Setup config.ini template
- [x] Use start_date and end_date for query
- [x] Provide script to convert json to tfrecords with contrib.data
- [x] Tensorflow estimator and dataset for text classification
- [ ] Provide script to convert json to fasttext format with pandas
