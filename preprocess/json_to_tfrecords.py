# -*- coding: utf-8 -*-
import json
from argparse import ArgumentParser
from os.path import splitext, basename

import spacy
from tqdm import tqdm

from helper import *

"""
This script is meant to preprocess json files in raw folder into tfrecords files
in preprocessed folder.

The feature words are converted into integers by using glove vocabulary. Note
that it is not straight forward as <pad> and <unk> tokens are added. --vec_dim
states which glove will be used, and --cutoff determines the length of feature
sequence.

Refer to this link for explanation on writing tfrecords files
http://www.machinelearninguru.com/deep_learning/tensorflow/basics/tfrecord/tfrecord.html
"""


GLOVE_FOLDER = os.path.join(".", "data", "interim", "glove.6B")
RAW_FOLDER = os.path.join(".", "data", "raw")
PREP_FOLDER = os.path.join(".", "data", "preprocessed")
BATCH_SIZE = 2000
NUM_CORES = 6


def main(vec_dim, cutoff):
    # Load glove dictionary
    glove_loc = os.path.join(GLOVE_FOLDER, "glove.6B.%dd.txt" % vec_dim)
    word_to_idx, _ = extract_glove_vocab(glove_loc)

    # Load spacy object for english
    nlp = spacy.load("en")

    # Load topics
    topics_loc = "./data/interim/topics.txt"
    with open(topics_loc, 'r') as f:
        topics = []
        for line in f.readlines():
            topics.append(line.strip())
    topics_to_idx = {topic: idx for idx, topic in enumerate(topics)}

    # Get all batch files in json
    json_files = get_batch_files(RAW_FOLDER, "json")
    for mode, batches in tqdm(json_files.items(),
                              desc="# mode"):
        for batch in tqdm(batches,
                          desc="# batch"):
            with open(batch, 'r') as f:
                samples = json.load(f)
            # Preprocess for sequence and label
            sequences = clean_text(samples, word_to_idx, nlp, BATCH_SIZE,
                                   NUM_CORES, cutoff=cutoff)
            num_labels = clean_label(samples, topics_to_idx)

            batchname_wo_ext = splitext(basename(batch))[0]

            # Write to tfrecords format
            target_path = os.path.join(PREP_FOLDER, mode,
                                       batchname_wo_ext + ".tfrecords")
            writer = tf.python_io.TFRecordWriter(target_path)
            for seq, lab in zip(sequences, num_labels):
                feature = {"sequence": sequence_feature(seq),
                           "label": label_feature(lab)}
                example = tf.train.Example(
                    features=tf.train.Features(feature=feature))
                writer.write(example.SerializeToString())
            writer.close()


if __name__ == '__main__':
    args = ArgumentParser(description="Convert json data into tfrecords")
    args.add_argument("--vec_dim",
                      default=50,
                      type=int,
                      action="store",
                      dest="vec_dim",
                      help="Embedding vector dimension")
    args.add_argument("--cutoff",
                      default=300,
                      type=int,
                      action="store",
                      dest="cutoff",
                      help="The length of a sample")
    args = args.parse_args()
    main(args.vec_dim, args.cutoff)