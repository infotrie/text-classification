# -*- coding: utf-8 -*-
import os
import tensorflow as tf


def get_batch_files(parent_folder, ext):
    """
    Get all batch files from parent_folder
    :param parent_folder: Location of parent folder for raw files
    :param ext: Extension of batch files
    :return: Dictionary with keys: train, test, and valid. The values are lists
     consisting paths to batch files.
    """
    batch_files = {"train": [],
                   "test": [],
                   "valid": []}
    for mode in batch_files.keys():
        mode_folder = os.path.join(parent_folder, mode)
        files = [os.path.join(mode_folder, f) for f in os.listdir(mode_folder)
                 if f.endswith(ext)]
        batch_files[mode].extend(files)
    return batch_files


def extract_glove_vocab(glove_path):
    """
    Get glove vocabulary words from glove file
    :param glove_path: Glove file location.
    :return: Tuple of dictionary and list. The dictionary consists of word
     as key and index as value, the list is the reverse of dictionary
    """
    idx_to_word = ["<unk>", "<pad>"]
    with open(glove_path, 'r') as f:
        for line in f.readlines():
            values = line.split(" ")
            idx_to_word.append(values[0])
    word_to_idx = {word: idx for idx, word in enumerate(idx_to_word)}
    return word_to_idx, idx_to_word


def pad_sequence(seq, max_len, pad_idx):
    """
    Ensure the length of a sequence is equal to max_len. Either the original
    sequence is cut off or padded.
    :param seq: List of integer
    :param max_len: The target length of sequence
    :param pad_idx: The element to be padded to the sequence
    :return: Cut off or padded sequence with length of max_len
    """
    if len(seq) > max_len:
        padded_seq = seq[:max_len]
    elif len(seq) < max_len:
        gap = max_len - len(seq)
        padded_seq = seq + [pad_idx] * gap
    else:
        padded_seq = seq
    return padded_seq



def clean_text(samples, word_to_idx, nlp, batch_size=1000, num_cores=2,
               cutoff=300):
    """
    Preprocess text data by combining title and text fields, then convert it
    into sequence of integer based on word_to_idx dictionary, and pad each
    sequence so that the length is equal to cutoff
    :param samples: List of dictionaries containing title and text keys
    :param word_to_idx: Dictionary where the key is word, and the value is
     integer
    :param nlp: Spacy object
    :param batch_size: The sample size to be processed by Spacy at once
    :param num_cores: The number of cores available to be used for preprocessing
    :param cutoff: The length of sequence after preprocessing
    :return: List of sequence. Each sequence consists of integer with length of
     cutoff
    """
    combined = [x["title"] + ". " + x["text"] for x in samples]
    sequences = []
    unk_index = word_to_idx["<unk>"]
    pad_index = word_to_idx["<pad>"]
    for doc in nlp.pipe(combined, batch_size, num_cores):
        tokenized = [token.text.lower() for token in doc][:cutoff]
        indexed = [word_to_idx.get(word, unk_index) for word in tokenized]
        padded = pad_sequence(indexed, cutoff, pad_index)
        sequences.append(padded)
    return sequences


def clean_label(samples, topic_to_idx):
    """
    Preprocess topic data by converting each topic into numerical data.
    :param samples: List of dictionaries containing topic key
    :param topic_to_idx: Dictionary where the key is topic and the value is
    integer
    :return: List of integer
    """
    numeric_labels = [topic_to_idx[x["topic"]] for x in samples]
    return numeric_labels


def sequence_feature(value):
    """
    Convert sequence to tensorflow Feature
    :param value: A sequence of integer
    :return: Tensorflow Feature for sequence
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def label_feature(value):
    """
    Convert an integer to tensorflow Feature
    :param value: An integer
    :return: Tensorflow Feature for sequence
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
