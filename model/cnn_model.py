# -*- coding: utf-8 -*-
import tensorflow as tf
from tf_dataset import load_embedding


def cnn_model(features, labels, mode, params):
    """
    CNN model for text classification. refer to this link for explanation
    http://www.wildml.com/2015/12/implementing-a-cnn-for-text-classification-in-tensorflow/
    :param features: Dictionary containing 'sequence'
    :param labels: One hot vector
    :param mode: Either TRAIN, EVAL, or PREDICT from tf.estimator.ModeKeys
    :param params: Dictionary of hyperparameters
    :return: tensorflow Estimator Spec
    """
    embedding_matrix = load_embedding(params["vec_dim"])
    embedding = tf.get_variable("embedding",
                                shape=embedding_matrix.shape,
                                initializer=tf.constant_initializer(embedding_matrix),
                                trainable=False)
    embed_layer = tf.nn.embedding_lookup(embedding, features["sequence"])
    embed_layer = tf.expand_dims(embed_layer, 3)

    pooled_outputs = []
    seq_length = features["sequence"].get_shape()[1]
    for i, kernel_size in enumerate(params["kernel_sizes"]):
        with tf.name_scope("CNN_Layer_%d" % i):
            conv = tf.layers.conv2d(embed_layer,
                                    filters=128,
                                    kernel_size=[kernel_size, params["vec_dim"]],
                                    padding="VALID",
                                    activation=tf.nn.relu)
            pooled = tf.layers.max_pooling2d(conv,
                                             pool_size=[int(seq_length) - kernel_size + 1, 1],
                                             strides=1,
                                             padding="VALID")
            pooled_outputs.append(pooled)
    num_filters_total = 128 * len(params["kernel_sizes"])
    h_pool = tf.concat(pooled_outputs, 3)
    h_pool_flat = tf.reshape(h_pool, [-1, num_filters_total])
    with tf.name_scope("dropout"):
        if mode == tf.estimator.ModeKeys.TRAIN:
            h_pool_flat = tf.layers.dropout(h_pool_flat, params["dropout"],
                                            training=True)

    with tf.name_scope("output"):
        logits = tf.layers.dense(h_pool_flat, params["num_topics"])

    predicted_labels = tf.argmax(logits, 1)
    # Prediction mode
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(
            mode=mode,
            predictions={
                "label": predicted_labels,
                "prob": tf.nn.softmax(logits)
            }
        )
    # Training mode
    loss = tf.losses.softmax_cross_entropy(labels, logits)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdagradOptimizer(learning_rate=params["learning_rate"])
        train_op = optimizer.minimize(loss,
                                      global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)

    # Evaluation mode
    single_labels = tf.argmax(labels, axis=1)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=single_labels, predictions=predicted_labels
        )
    }
    return tf.estimator.EstimatorSpec(mode=mode,
                                      loss=loss,
                                      eval_metric_ops=eval_metric_ops)
