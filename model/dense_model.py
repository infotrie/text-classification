# -*- coding: utf-8 -*-
import tensorflow as tf
from tf_dataset import load_embedding, dataset_input_fn
tf.logging.set_verbosity(tf.logging.INFO)


def dense_model(features, labels, mode, params):
    """
    Simple classification model with linear layer
    :param features: Dictionary containing 'sequence'
    :param labels: One hot vector
    :param mode: Either TRAIN, EVAL, or PREDICT from tf.estimator.ModeKeys
    :param params: Dictionary of hyper-parameters
    :return: tensorflow EstimatorSpec
    """
    # Embed the sequence by using glove vector
    embedding_matrix = load_embedding(params["vec_dim"])
    embedding = tf.get_variable("embedding",
                                shape=embedding_matrix.shape,
                                initializer=tf.constant_initializer(embedding_matrix),
                                trainable=False)
    embed_layer = tf.nn.embedding_lookup(embedding, features["sequence"])

    # Flatten
    seq_length = features["sequence"].get_shape()[1]
    flattened = tf.reshape(embed_layer, (-1, params["vec_dim"] * int(seq_length)))

    # Apply linear layer
    linear_1 = tf.layers.dense(flattened, 128, activation=tf.nn.relu)
    logits = tf.layers.dense(linear_1, params["num_topics"])

    predicted_labels = tf.argmax(logits, 1)
    # Prediction mode
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode,
                                          predictions={
                                              "label": predicted_labels,
                                              "prob": tf.nn.softmax(logits)})
    # Training mode
    loss = tf.losses.softmax_cross_entropy(labels, logits)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(params["learning_rate"])
        train_op = optimizer.minimize(loss,
                                      global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode,
                                          loss=loss,
                                          train_op=train_op)

    # Evaluation mode
    single_labels = tf.argmax(labels, 1)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(single_labels, predicted_labels)
    }
    return tf.estimator.EstimatorSpec(mode=mode,
                                      loss=loss,
                                      eval_metric_ops=eval_metric_ops)
