# -*- coding: utf-8 -*-
from pprint import pprint

from tf_dataset import _num_topics, dataset_input_fn
from cnn_model import cnn_model
from dense_model import dense_model
from argparse import ArgumentParser
import tensorflow as tf


tf.logging.set_verbosity(tf.logging.INFO)

"""
Basic script to train and evaluate text-classification model. Tensorflow
Estimator is used as framework to train and evaluate model.
"""

def train(type, vec_dim, log_dir, num_topics, num_steps):
    """
    Train neural net
    :param vec_dim: Embedding word vector dimension
    :param log_dir: Folder to store model training checkpoint
    :param num_topics: Number of classes
    :param num_steps: Number of steps in training. One step represent one
    mini-batch
    :return: Trained neural net
    """
    if type == "cnn":
        nn = tf.estimator.Estimator(model_fn=cnn_model,
                                    model_dir=log_dir,
                                    params={"vec_dim": 50,
                                            "num_topics": num_topics,
                                            "kernel_sizes": [3, 4, 5],
                                            "dropout": 0.5,
                                            "learning_rate": 0.001})
    elif type == "dense":
        nn = tf.estimator.Estimator(model_fn=dense_model,
                                    model_dir=log_dir,
                                    params={"vec_dim": 50,
                                            "num_topics": num_topics,
                                            "learning_rate": 0.001})
    else:
        raise ValueError("Model not found")
    nn.train(input_fn=lambda: dataset_input_fn("train", repeat=True),
             steps=num_steps)
    return nn


def evaluate(neural_net):
    """
    Compute accuracy on train and valid dataset
    :param neural_net: Trained neural net
    :return: Accuracy results on train and valid data
    """
    training_result = neural_net.evaluate(
        input_fn=lambda: dataset_input_fn("train", repeat=False, shuffle_size=None))
    validation_result = neural_net.evaluate(
        input_fn=lambda: dataset_input_fn("valid", repeat=False, shuffle_size=None)
    )
    return {"train": training_result,
            "valid": validation_result}


if __name__ == '__main__':
    args = ArgumentParser(description="Train text classification model")
    args.add_argument("--vec_dim",
                      type=int,
                      default=50,
                      dest="vec_dim",
                      help="Word embedding vector dimension")

    args.add_argument("--log_dir",
                      type=str,
                      default="model_log",
                      dest="log_dir",
                      help="Folder to store tensorflow log while "
                                  "training")

    args.add_argument("--steps",
                      type=int,
                      default=5000,
                      dest="steps",
                      help="Number of training steps")

    args.add_argument("--model_type",
                      type=str,
                      default="cnn",
                      dest="model_type",
                      help="Model type, either dense or CNN")

    parsed_args = args.parse_args()
    num_topics = _num_topics()
    nn = train(parsed_args.model_type, parsed_args.vec_dim, parsed_args.log_dir,
               num_topics, parsed_args.steps)
    pprint(evaluate(nn))
