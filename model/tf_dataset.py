# -*- coding: utf-8 -*-
import os
import tensorflow as tf
import numpy as np


EMBEDDING_FOLDER = os.path.join(".", "data", "interim", "glove.6B")
TOPICS_FILE = os.path.join(".", "data", "interim", "topics.txt")
PREP_FOLDER = os.path.join(".", "data", "preprocessed")


def _num_topics():
    """
    Count how many topics in the TOPICS_FILE
    :return: Number of topics
    """
    i = 0
    with open(TOPICS_FILE, "r") as f:
        for i, _ in enumerate(f, 1):
            pass
    return i


def load_embedding(vec_dim):
    """
    Load embedding vector in EMBEDDING_FOLDER
    :param vec_dim: Word embedding vector dimension
    :return: Numpy array of word embedding, the shape is vocab size x vec_dim
    """
    # Embedding matrix, where the first two rows are for <unk> and <pad>
    embd = [[0.] * vec_dim, [0.] * vec_dim]

    # Read glove vectors, and convert it as numpy array
    with open(os.path.join(EMBEDDING_FOLDER, "glove.6B.%dd.txt" % vec_dim), "r") as f:
        for line in f.readlines():
            row = line.strip().split(' ')
            embd.append(row[1:])
    embedding = np.asarray(embd, dtype=np.float32)
    del embd
    return embedding


def dataset_input_fn(mode, shuffle_size=5000, batch_size=64, repeat=True):
    """
    Input function for tensorflow estimator
    :param mode: train, valid, or test
    :param shuffle_size: The buffer size for shuffling the data. Input None if
    there is no shuffling
    :param batch_size: The size of a mini-batch
    :param repeat: Repeat the data or not
    :return: Tuple of dictionary and label. The dictionary's key is "sequence",
    which contains sequence feature. The label is one-hot vector.
    """
    num_topics = _num_topics()
    mode_folder = os.path.join(PREP_FOLDER, mode)
    filenames = [os.path.join(mode_folder, f) for f in os.listdir(mode_folder)
                 if f.endswith("tfrecords")]
    dataset = tf.contrib.data.TFRecordDataset(filenames)

    def parser(record):
        keys_to_features = {"sequence":tf.FixedLenFeature([300], dtype=tf.int64),
                            "label": tf.FixedLenFeature([], dtype=tf.int64)}
        parsed = tf.parse_single_example(record, keys_to_features)
        one_hot_label = tf.one_hot(indices=parsed["label"], depth=num_topics,
                                   dtype=tf.int64)
        return ({"sequence": parsed["sequence"]},one_hot_label)
    dataset = dataset.map(parser)
    if shuffle_size:
        dataset = dataset.shuffle(buffer_size=shuffle_size)
    dataset = dataset.batch(batch_size)
    if repeat:
        dataset = dataset.repeat()
    iterator = dataset.make_one_shot_iterator()
    sequences, labels = iterator.get_next()
    return sequences, labels
