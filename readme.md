# Introduction

Template for tensorflow project

# Installation

1. Install python module dependencies in requirements.txt
2. Install [spacy english model](https://spacy.io/docs/usage/models)
3. Download [glove files](http://nlp.stanford.edu/data/glove.6B.zip) and extract
it to data/interim folder
4. Rename config.ini.template to config.ini, then fill mongo uri field

# Usage
* Always run the script from project root folder
* Check each script source code, or run with `--help` for custom configuration
* To query data and store it in data/raw, run download_script/download_from_mongo
script  
`python ./download_script/download_from_mongo.py`
* To preprocess raw data by tokenizing text, convert it into integer index by using
glove words, run preprocess/json_to_tfrecords script  
`python ./preprocess/json_to_tfrecords.py`
* To train text-classification model, run model/train_model script  
`python ./model/train_model.py`